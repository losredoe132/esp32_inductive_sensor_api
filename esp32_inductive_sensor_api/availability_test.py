import requests
import json
import time

success_list = []
delta_t_list = []

n_try = 1000
for i in range(n_try):
    t_0 = time.time()
    try:
        r = requests.get("http://141.70.223.209/sensor", timeout=1)
        success_list.append(True)
        print(f"{i}: {r.content.decode('utf-8')}")

    except Exception as e:
        success = False
        success_list.append(False)
        print(f"{i}: Fail!")

    delta_t_list.append(time.time()-t_0)
    time.sleep(2)


#
n_success = success_list.count(True)
n_overall = len(success_list)
success_rate = n_success/n_overall
print(f"{n_success} / {n_overall} successfully requested ({success_rate*100} %)")
